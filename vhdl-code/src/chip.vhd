--
-- Entity: chip
-- Architecture : structural
-- Author: sgraff2
-- Created on 11/11/2017
--

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity chip is
    port(
        cpu_add    : in std_logic_vector(7 downto 0);
        cpu_data   : inout std_logic_vector(7 downto 0);
        cpu_rd_wrn : in std_logic;
        start      : in std_logic;
        clk        : in std_logic;
        reset      : in std_logic;
        mem_data   : in std_logic_vector(7 downto 0);
        Vdd        : in std_logic;
        Gnd        : in std_logic;
        busy       : out std_logic;
        mem_en     : out std_logic;
        mem_add    : out std_logic_vector(7 downto 0));
end chip;

architecture structural of chip is

-- inverter
component inverter

  port (
    input    : in  std_logic;
    output   : out std_logic);
end component;


-- and2
component and2

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

-- mux_2to1
component mux_2to1
    port(
        sel     : in std_logic;
        in0     : in std_logic_vector(1 downto 0);
        in1     : in std_logic_vector(1 downto 0);
        output  : out std_logic_vector(1 downto 0));
end component;   

-- hitmiss
component hitmiss
    port(
        in1    : in  std_logic_vector(2 downto 0);
        in2    : in  std_logic_vector(2 downto 0);
        valid  : in  std_logic;
        hit    : out std_logic );
end component;

-- tx
component tx                    
  port ( sel   : in std_logic;
         selnot: in std_logic;
         input : in std_logic;
         output:out std_logic);
end component;

-- tx8
component tx8                     
  port ( sel   : in std_logic;
         selnot: in std_logic;
         input : in std_logic_vector(7 downto 0);
         output:out std_logic_vector(7 downto 0));
end component;                          

-- dff
component dff                      
  port ( d   : in  std_logic;
         clk : in  std_logic;
         q   : out std_logic;
         qbar: out std_logic); 
end component;                          

-- register8
component register8                      
  port ( clk    : in  std_logic;
         input  : in std_logic_vector(7 downto 0);
         output : out std_logic_vector(7 downto 0)); 
end component;                          

-- cache
component cache
port (
    inputData      : in  std_logic_vector(7 downto 0);
    offBlock    : in  std_logic_vector(2 downto 0);
    offByte     : in  std_logic_vector(1 downto 0);
    tagIn       : in  std_logic_vector(2 downto 0);
    reset		: in  std_logic;
    clk		: in  std_logic;
    write	: in  std_logic;
    valid	: out std_logic;
    tagOut	: out std_logic_vector(2 downto 0);
    outputData	: out std_logic_vector(7 downto 0));	
end component;

-- stateMachine
component stateMachine
    port(
        clk     : in std_logic;
        start   : in std_logic;
        hitMiss : in std_logic;
        read    : in std_logic;
        write   : in std_logic;
        memEn   : out std_logic;
        cacheAd : out std_logic_vector(1 downto 0);
        cacheWr : out std_logic;
        CPUoutEn: out std_logic;
        busy    : out std_logic);
end component;


for invdataout, invmiss, invstart: inverter use entity work.inverter(structural);
for andDataclk, andrdmiss: and2 use entity work.and2(structural);
for muxbyte: mux_2to1 use entity work.mux_2to1(structural);
for hitMiss0: hitmiss use entity work.hitmiss(structural);
for tx8_dataIn, tx8memData, tx8dataOut: tx8 use entity work.tx8(structural);
for dffrw, dffhit: dff use entity work.dff(structural);
for reg8ad, reg8data: register8 use entity work.register8(structural);
for cache1: cache use entity work.cache(structural);
for stateMachine1: stateMachine use entity work.stateMachine(structural);
for txhitd, txhit: tx use entity work.tx(structural);
for inv0, inv1, inv2, inv3, inv4, inv5, inv6, inv7 : inverter use entity work.inverter(structural);
for inv8, inv9, inv10, inv11, inv12, inv13, inv14, inv15 : inverter use entity work.inverter(structural);
for inv16, inv17, inv18, inv19 : inverter use entity work.inverter(structural);

signal dataClk, read, write, hitt, hit, miss, rdMiss, cacheWr, outputEn, outputEnn, valid: std_logic;
signal tempVdd, tempGnd, tempVddn, tempGndn, hitd, startn: std_logic;
signal ad, dataIn, dataInt, dataOutt: std_logic_vector (7 downto 0);
signal tagOut: std_logic_vector (2 downto 0);
signal RMByte, cacheByteOff: std_logic_vector(1 downto 0);
signal buf_add : std_logic_vector (7 downto 0);


begin

    andDataclk  : and2 port map (start, clk, dataClk);

    -- latch the rd/wr and wr/rd signals
    dffrw       : dff  port map (cpu_rd_wrn, dataClk, read, write);
    
    -- latch the address
    reg8ad	: register8 port map (dataClk, cpu_add, ad);
    reg8data	: register8 port map (dataClk, cpu_data , dataInt);

    -- get the data coming in on write 
    tx8_dataIn	: tx8  port map (write, read, dataInt, dataIn); 
    -- get the data comming from memory on read
    tx8memData	: tx8	port map (read, write, mem_data, dataIn);

    invstart	: inverter port map (start, startn);
    -- calculate the hit
    hitMiss0	: hitmiss port map (ad (7 downto 5), tagOut, valid, hitt);
    dffhit	: dff port map (hitt, start, hitd,  open);

    -- if it's a miss, use the registered value
    txhitd	: tx port map (startn, start, hitd, hit);

    -- otherwise, don't use it
    txhit	: tx port map (start, startn, hitt, hit);
    invmiss	: inverter port map (hit, miss);	
    
    -- send all the signals to the statemachine
    stateMachine1	: stateMachine	port map (clk, start, hit, read, write, mem_en, RMByte, cacheWr, outputEn, busy);

    -- let the cpu bus see the data
    invdataout	: inverter	port map (outputEn, outputEnn);

    tx8dataOut	: tx8		port map (outputEn, outputEnn, dataOutt, cpu_data);

    -- select the byte offset to send to the cache
    andrdmiss	: and2		port map (read, miss, rdMiss);
    muxbyte	: mux_2to1	port map (rdMiss, ad(1 downto 0), RMByte, cacheByteOff); 

    -- send all the signals to the cache
    cache1	: cache		port map (dataIn, ad(4 downto 2), cacheByteOff, ad(7 downto 5), reset, clk, cacheWr, valid, tagOut, dataOutt);

    -- invert all the bits for the signals we care about
    inv0		: inverter	port map (ad(0), buf_add(0));
    inv1		: inverter	port map (ad(1), buf_add(1));
    inv2		: inverter	port map (ad(2), buf_add(2));
    inv3		: inverter	port map (ad(3), buf_add(3));
    inv4		: inverter	port map (ad(4), buf_add(4));
    inv5		: inverter	port map (ad(5), buf_add(5));
    inv6		: inverter	port map (ad(6), buf_add(6));
    inv7		: inverter	port map (ad(7), buf_add(7));

    inv8		: inverter	port map (buf_add(0), mem_add(0));
    inv9		: inverter	port map (buf_add(1), mem_add(1));
    inv10		: inverter	port map (buf_add(2), mem_add(2));
    inv11		: inverter	port map (buf_add(3), mem_add(3));
    inv12		: inverter	port map (buf_add(4), mem_add(4));
    inv13		: inverter	port map (buf_add(5), mem_add(5));
    inv14		: inverter	port map (buf_add(6), mem_add(6));
    inv15		: inverter	port map (buf_add(7), mem_add(7));

    inv16		: inverter	port map (Vdd, tempVddn);
    inv17		: inverter	port map (tempVddn, tempVdd);
    inv18		: inverter	port map (Gnd, tempGndn);
    inv19		: inverter	port map (tempGndn, tempGnd);

end structural;
