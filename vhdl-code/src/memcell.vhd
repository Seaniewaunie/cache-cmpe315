-- Entity: memcell
-- Architecture: structural
-- Author: mousta1
-- Date: 11/08/2017

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity memCell is

  port (
    input   : in  std_logic_vector(7 downto 0);	
    rowEn	: in  std_logic; 
    colEn	: in  std_logic; 
    wr   	: in  std_logic;				
    output  : out std_logic_vector(7 downto 0));

end memCell;

architecture structural of memCell is


component inverter
  port (
    input	: in std_logic;
    output	: out std_logic);
  end component;

component nand2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
  end component;

component tx
  port(
     sel	: in std_logic;
     selnot	: in std_logic;
     input	: in std_logic;
     output	: out std_logic);
  end component;
 
 component latch                  
  port (          
        clk : in  std_logic;
        clkn : in  std_logic;
        d   : in  std_logic;
        q   : out std_logic;
        qbar: out std_logic); 
end component;                          



for inv, invRd : inverter use entity work.inverter(structural);
for nandEn, nandRd : nand2 use entity work.nand2(structural);

for tx_0, tx_1, tx_2, tx_3, tx_4, tx_5, tx_6, tx_7: tx use entity work.tx(structural);
for latch_0, latch_1, latch_2, latch_3, latch_4, latch_5, latch_6, latch_7: latch use entity work.latch(structural);

signal en, enn, wrowEn, wrowEnn, rdn: std_logic;
signal outputT: std_logic_vector (7 downto 0);

signal temp : std_logic;

begin


  nandEn: nand2   port map (rowEn, colEn, enn);  --See if this is the correct block
  inv:   inverter port map (enn, en);

  nandRd: nand2   port map (en, wr, wrowEnn);  
  invRd: inverter port map (wrowEnn, wrowEn);

  
  latch_0: latch port map (wrowEn, wrowEnn, input(0), outputT(0), temp);
  latch_1: latch port map (wrowEn, wrowEnn, input(1), outputT(1), temp);
  latch_2: latch port map (wrowEn, wrowEnn, input(2), outputT(2), temp);
  latch_3: latch port map (wrowEn, wrowEnn, input(3), outputT(3), temp);
  latch_4: latch port map (wrowEn, wrowEnn, input(4), outputT(4), temp);
  latch_5: latch port map (wrowEn, wrowEnn, input(5), outputT(5), temp);
  latch_6: latch port map (wrowEn, wrowEnn, input(6), outputT(6), temp);
  latch_7: latch port map (wrowEn, wrowEnn, input(7), outputT(7), temp);
  
   --If this is the block, let output
    
  tx_0: tx port map (en, enn, outputT(0), output(0));  
  tx_1: tx port map (en, enn, outputT(1), output(1));
  tx_2: tx port map (en, enn, outputT(2), output(2));
  tx_3: tx port map (en, enn, outputT(3), output(3));
  tx_4: tx port map (en, enn, outputT(4), output(4)); 
  tx_5: tx port map (en, enn, outputT(5), output(5));
  tx_6: tx port map (en, enn, outputT(6), output(6)); 
  tx_7: tx port map (en, enn, outputT(7), output(7));
end structural;
