-- header TODO



library STD;
library IEEE;                      
use IEEE.std_logic_1164.all;       

entity latch is                      
  port (          
        clk : in  std_logic;
        clkn : in  std_logic;
        d   : in  std_logic;
        q   : out std_logic;
        qbar: out std_logic); 
end latch;                          

architecture structural of latch is 

 
  
begin
  
  output: process (d,clk)                  

  begin                           
    if clk = '1' then 
    q <= d;
    qbar <= not d ;
 end if; 
 end process output;        
                             
end structural;  
