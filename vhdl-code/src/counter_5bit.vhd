-- Entity: counter_5bit
-- Architecture: structural
-- Author: mousta1
-- Date: 11/08/2017

library STD;
library IEEE;                      
use IEEE.std_logic_1164.all;       

entity counter_5bit is                      
  port ( 
     count 	 : in  std_logic;
     reset	 : in  std_logic;
     Clk  	 : in std_logic;
     output  : out  std_logic_vector(4 downto 0);
     outputn : out std_logic_vector(4 downto 0)); 
end counter_5bit;                          

architecture structural of counter_5bit is

component inverter
  port ( input	: in std_logic;
	 output	: out std_logic);
end component;

component and2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

component xor2
  port (
    input1   : in std_logic;
    input2   : in std_logic;
    output   : out std_logic);
end component;

component dff
  port ( d   : in  std_logic;
         clk : in  std_logic;
         q   : out std_logic;
         qbar: out std_logic); 
end component;


for inv_1										: inverter use entity work.inverter(structural);
for and2_c1, and2_c2, and2_c3, and2_c4 			: and2 use entity work.and2(structural);
for and2_r1, and2_r2, and2_r3, and2_r4, and2_r5 : and2 use entity work.and2(structural);
for xor2_c1, xor2_c2, xor2_c3, xor2_c4, xor2_c5	: xor2 use entity work.xor2(structural);
for dff_1, dff_2, dff_3, dff_4, dff_5			: dff use entity work.dff(structural);


for bufinv_0, bufinv_1, bufinv_2, bufinv_3, bufinv_4, bufinv_5, bufinv_6, bufinv_7, bufinv_8, bufinv_9 : inverter use entity work.inverter(structural);

signal D				: std_logic_vector(4 downto 0);
signal R 				: std_logic_vector(4 downto 0);
signal count1, count2, count3, count4 	: std_logic;
signal temp 	 		: std_logic_vector(4 downto 0); --holds the outputn
signal resetn 	 		: std_logic;
signal outputemp 		: std_logic_vector(4 downto 0);
signal buff 	 		: std_logic_vector(4 downto 0);

begin

-- Determines if the counter counts or not
and2_c1: 	and2 port map ( count, 	outputemp(0), count1 );
and2_c2: 	and2 port map ( count1, outputemp(1), count2 );
and2_c3: 	and2 port map ( count2, outputemp(2), count3 );
and2_c4: 	and2 port map ( count3, outputemp(3), count4 );

xor2_c1: 	xor2 port map ( count, 	outputemp(0), D(0) );
xor2_c2: 	xor2 port map ( count1, outputemp(1), D(1) );
xor2_c3: 	xor2 port map ( count2, outputemp(2), D(2) );
xor2_c4: 	xor2 port map ( count3, outputemp(3), D(3) );
xor2_c5: 	xor2 port map ( count4, outputemp(4), D(4) );

-- Determines if the counter resets

inv_1: inverter port map ( reset, resetn );

and2_r1: 	and2 port map ( D(0), resetn, R(0) );
and2_r2: 	and2 port map ( D(1), resetn, R(1) );
and2_r3: 	and2 port map ( D(2), resetn, R(2) );
and2_r4: 	and2 port map ( D(3), resetn, R(3) );
and2_r5: 	and2 port map ( D(4), resetn, R(4) );

-- Flipflops hold output

dff_1:	dff port map ( R(0), Clk, outputemp(0), outputn(0) );
dff_2:	dff port map ( R(1), Clk, outputemp(1), outputn(1) );
dff_3:	dff port map ( R(2), Clk, outputemp(2), outputn(2) );
dff_4:	dff port map ( R(3), Clk, outputemp(3), outputn(3) );
dff_5:	dff port map ( R(4), Clk, outputemp(4), outputn(4) );

--Buffer the output rather than --output <= outputemp;
bufinv_0: inverter port map (outputemp(0), buff(0));
bufinv_1: inverter port map (outputemp(1), buff(1));
bufinv_2: inverter port map (outputemp(2), buff(2));
bufinv_3: inverter port map (outputemp(3), buff(3));
bufinv_4: inverter port map (outputemp(4), buff(4));

bufinv_5: inverter port map (buff(0), output(0));
bufinv_6: inverter port map (buff(1), output(1));
bufinv_7: inverter port map (buff(2), output(2));
bufinv_8: inverter port map (buff(3), output(3));
bufinv_9: inverter port map (buff(4), output(4));

end structural;