-- Entity: memBlock
-- Architecture: structural
-- Author: mousta1
-- Date: 11/08/2017

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity memBlock is
  port(
    inputData   : in std_logic_vector(7 downto 0);
    blockEn     : in std_logic;
    blockEnn    : in std_logic;
    byteEn      : in std_logic_vector(3 downto 0);
    inputTag       : in std_logic_vector(2 downto 0);
    rstn        : in std_logic;
    rstandclk   : in std_logic;
    wr    	: in std_logic;
    valid       : out std_logic;
    outputTag      : out std_logic_vector(2 downto 0);
    outputByData0  : out std_logic_vector(7 downto 0);    
    outputByData1  : out std_logic_vector(7 downto 0);
    outputByData2  : out std_logic_vector(7 downto 0);
    outputByData3  : out std_logic_vector(7 downto 0));
end memBlock;

architecture structural of memBlock is

component inverter
  port (
    input	: in std_logic;
    output	: out std_logic);
  end component;

component and2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
  end component;

component nor2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
  end component;
  
component latch                 
  port (          
        clk : in  std_logic;
        clkn : in  std_logic;
        d   : in  std_logic;
        q   : out std_logic;
        qbar: out std_logic); 
end component;                          

component tx	
  port(
     sel	: in std_logic;
     selnot	: in std_logic;
     input	: in std_logic;
     output	: out std_logic);
 end component;
 


component memCell
  port (
    input   : in  std_logic_vector(7 downto 0);	
    rowEn	: in  std_logic;		
    colEn	: in  std_logic;	
    wr   	: in  std_logic;				
    output  : out std_logic_vector(7 downto 0));
  end component;

for invvalEn	: inverter use entity work.inverter(structural);
for and2valEn 	: and2 use entity work.and2(structural);
for nor2valEn	: nor2 use entity work.nor2(structural);

for txval, txtag_0, txtag_1, txtag_2	: tx use entity work.tx(structural);

for latchtag_0	: latch use entity work.latch(structural);
for latchtag_1	: latch use entity work.latch(structural);
for latchtag_2	: latch use entity work.latch(structural);
for latchval 	: latch use entity work.latch(structural);

for mem_0, mem_1, mem_2, mem_3: memCell use entity work.memCell(structural);

signal valEnn, valEn, validt, wrVal	: std_logic;
signal valTagOut	: std_logic_vector(2 downto 0);


begin

    -- enable valid and tag writing
    and2valEn   : and2 port map (wr, blockEn, wrVal);
    nor2valEn   : nor2 port map (wrVal, rstandclk, valEnn);
    invvalEn    : inverter port map (valEnn, valEn);

    -- valid bit
    latchval    : latch	port map (valEn, valEnn, rstn, validt, open);
    txval       : tx port map (blockEn, blockEnn, validt, valid );

    -- tag
    latchtag_0  : latch	port map (valEn, valEnn, inputTag(0), valTagOut(0), open);
    latchtag_1	: latch	port map (valEn, valEnn, inputTag(1), valTagOut(1), open);
    latchtag_2	: latch	port map (valEn, valEnn, inputTag(2), valTagOut(2), open);

    txtag_0     : tx port map (blockEn, blockEnn, valTagOut(0), outputTag(0));
    txtag_1     : tx port map (blockEn, blockEnn, valTagOut(1), outputTag(1));
    txtag_2     : tx port map (blockEn, blockEnn, valTagOut(2), outputTag(2));
        
    mem_0       : memCell port map (inputData, blockEn, byteEn(0), wr, outputByData0);
    mem_1       : memCell port map (inputData, blockEn, byteEn(1), wr, outputByData1);
    mem_2       : memCell port map (inputData, blockEn, byteEn(2), wr, outputByData2);
    mem_3       : memCell port map (inputData, blockEn, byteEn(3), wr, outputByData3);
                
end structural;
