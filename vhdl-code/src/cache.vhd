--
-- Entity: cache
-- Architecture : structural
-- Author: sgraff2
-- Created on 11/09/2017
--

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity cache is
port (
    inputData      : in  std_logic_vector(7 downto 0);
    offBlock    : in  std_logic_vector(2 downto 0);
    offByte     : in  std_logic_vector(1 downto 0);
    tagIn       : in  std_logic_vector(2 downto 0);
    reset		: in  std_logic;
    clk		: in  std_logic;
    write	: in  std_logic;
    valid	: out std_logic;
    tagOut	: out std_logic_vector(2 downto 0);
    outputData	: out std_logic_vector(7 downto 0));	
end cache;

architecture structural of cache is

component inverter
  port (
    input	: in std_logic;
    output	: out std_logic);
end component;
    
component and2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;
    
component colEn

  port (
    sel	        : in  std_logic_vector(1 downto 0);
    col0	: out  std_logic;
    col1        : out  std_logic;
    col2        : out  std_logic;
    col3	: out  std_logic);
end component;


component rowEn

  port (
    sel	  : in  std_logic_vector(2 downto 0);
    row0  : out  std_logic;
    row1  : out  std_logic;
    row2  : out  std_logic;
    row3  : out  std_logic;
    row4  : out  std_logic;
    row5  : out  std_logic;
    row6  : out  std_logic;
    row7  : out  std_logic);
end component;

component memBlock
  port(
    inputData   : in std_logic_vector(7 downto 0);
    blockEn     : in std_logic;
    blockEnn    : in std_logic;
    byteEn      : in std_logic_vector(3 downto 0);
    inputTag       : in std_logic_vector(2 downto 0);
    rstn        : in std_logic;
    rstandclk   : in std_logic;
    wr    	: in std_logic;
    valid       : out std_logic;
    outputTag      : out std_logic_vector(2 downto 0);
    outputByData0  : out std_logic_vector(7 downto 0);    
    outputByData1  : out std_logic_vector(7 downto 0);
    outputByData2  : out std_logic_vector(7 downto 0);
    outputByData3  : out std_logic_vector(7 downto 0));
end component;

component tx
  port (
  	sel :	in std_logic;
  	selnot: in std_logic;
  	input :	in std_logic;
  	output:	out std_logic);
 end component;

for invreset : inverter use entity work.inverter(structural);
for invr_0, invr_1, invr_2, invr_3, invr_4, invr_5, invr_6, invr_7: inverter use entity work.inverter(structural);
for rowEn1: rowEn use entity work.rowEn(structural);
for colEn1: colEn use entity work.colEn(structural);

for andresetclk  : and2 use entity work.and2(structural);

for mem_0, mem_1, mem_2, mem_3, mem_4, mem_5, mem_6, mem_7 : memBlock use entity work.memBlock(structural);

for txtagIn_0, txtagIn_1, txtagIn_2, txtagIn_3, txtagIn_4, txtagIn_5 : tx use entity work.tx(structural); 

signal byteEn 	: std_logic_vector(3 downto 0);
signal tagInt : std_logic_vector(2 downto 0);

signal blockEn	: std_logic_vector(7 downto 0);
signal blockEnn : std_logic_vector(7 downto 0);
signal resetn, resetEn, resetEnn, resetandclk	: std_logic;

begin
  
  rowEn1:	rowEn port map (offBlock, BlockEn(0), BlockEn(1), BlockEn(2), BlockEn(3), BlockEn(4), BlockEn(5), BlockEn(6), BlockEn(7));
  invr_0: 	inverter port map (BlockEn(0), blockEnn(0));
  invr_1: 	inverter port map (BlockEn(1), blockEnn(1));
  invr_2: 	inverter port map (BlockEn(2), blockEnn(2));
  invr_3: 	inverter port map (BlockEn(3), blockEnn(3));
  invr_4: 	inverter port map (BlockEn(4), blockEnn(4));
  invr_5: 	inverter port map (BlockEn(5), blockEnn(5));
  invr_6: 	inverter port map (BlockEn(6), blockEnn(6));
  invr_7: 	inverter port map (BlockEn(7), blockEnn(7));  
  
  colEn1:	colEn port map (offByte, byteEn(0), byteEn(1), byteEn(2), byteEn(3));

    --input for valid bit
  invreset: inverter port map (reset, resetn);
  
  --setup tagIn so that will be zero at reset
  txtagIn_0:	tx port map 	( resetn, reset, tagIn(0), tagInt(0));
  txtagIn_1:	tx port map 	( resetn, reset, tagIn(1), tagInt(1));  
  txtagIn_2:	tx port map 	( resetn, reset, tagIn(2), tagInt(2));
  txtagIn_3:	tx port map 	( reset, resetn, resetn, tagInt(0));
  txtagIn_4:	tx port map 	( reset, resetn, resetn, tagInt(1));
  txtagIn_5:	tx port map 	( reset, resetn, resetn, tagInt(2));
  
  andresetclk : 	and2 port map	( clk, reset, resetandclk);
    
  --Memory modules
  --All of the data outputs are tied together because only one will have an output not z.
  mem_0:	     memBlock port map  (inputData, blockEn(0), blockEnn(0), byteEn, tagInt, resetn, resetandclk, write, valid, tagOut, outputData, outputData, outputData, outputData);
  mem_1:	     memBlock port map  (inputData, blockEn(1), blockEnn(1), byteEn, tagInt, resetn, resetandclk, write, valid, tagOut, outputData, outputData, outputData, outputData);
  mem_2:	     memBlock port map  (inputData, blockEn(2), blockEnn(2), byteEn, tagInt, resetn, resetandclk, write, valid, tagOut, outputData, outputData, outputData, outputData);
  mem_3:	     memBlock port map  (inputData, blockEn(3), blockEnn(3), byteEn, tagInt, resetn, resetandclk, write, valid, tagOut, outputData, outputData, outputData, outputData);
  mem_4:	     memBlock port map  (inputData, blockEn(4), blockEnn(4), byteEn, tagInt, resetn, resetandclk, write, valid, tagOut, outputData, outputData, outputData, outputData);  
  mem_5:	     memBlock port map  (inputData, blockEn(5), blockEnn(5), byteEn, tagInt, resetn, resetandclk, write, valid, tagOut, outputData, outputData, outputData, outputData);  
  mem_6:	     memBlock port map  (inputData, blockEn(6), blockEnn(6), byteEn, tagInt, resetn, resetandclk, write, valid, tagOut, outputData, outputData, outputData, outputData);  
  mem_7:	     memBlock port map  (inputData, blockEn(7), blockEnn(7), byteEn, tagInt, resetn, resetandclk, write, valid, tagOut, outputData, outputData, outputData, outputData);

end structural;
