--
-- Entity: stateMachine
-- Architecture : structural
-- Author: sgraff2
-- Created On: 11/11/2017
--

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity stateMachine is
    port(
        clk     : in std_logic;
        start   : in std_logic;
        hitMiss : in std_logic;
        read    : in std_logic;
        write   : in std_logic;
        memEn   : out std_logic;
        cacheAd : out std_logic_vector(1 downto 0);
        cacheWr : out std_logic;
        CPUoutEn: out std_logic;
        busy    : out std_logic);
end stateMachine;

architecture structural of stateMachine is

-- inverter
component inverter

  port (
    input    : in  std_logic;
    output   : out std_logic);
end component;

-- and2
component and2

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

-- and3
component and3

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    input3   : in  std_logic;
    output   : out std_logic);
end component;

-- or2
component or2
	port(
		input1	: in std_logic;
		input2	: in std_logic;
		output	: out std_logic);
end component;

-- counter_5bit
component counter_5bit
  port ( 
     count 	 : in  std_logic;
     reset	 : in  std_logic;
     Clk  	 : in std_logic;
     output  : out  std_logic_vector(4 downto 0);
     outputn : out std_logic_vector(4 downto 0)); 
end component;                          

-- latch
component latch                  
  port (          
        clk : in  std_logic;
        clkn : in  std_logic;
        d   : in  std_logic;
        q   : out std_logic;
        qbar: out std_logic); 
end component;                          

-- dff
component dff
  port ( d   : in  std_logic;
         clk : in  std_logic;
         q   : out std_logic;
         qbar: out std_logic); 
end component;                          


for invmiss, invt18, invstart: inverter use entity work.inverter(structural);

for andrm, andb1, andRMt18n: and2 use entity work.and2(structural);
for andmemen: and2 use entity work.and2(structural);
for andb00, andb10, andb11, andstclk: and2 use entity work.and2(structural);

for andt20, andt00, andt10, andt161, andt181, andcacheWr2: and3 use entity work.and3(structural);
for andt120, andt121, andt160, andcpuout, andb03, andcache2: and3 use entity work.and3(structural);

for ort0wt1, orbusy, orcache3: or2 use entity work.or2(structural);
for orb01, orb02, orb12, orb13, orbsyclk: or2 use entity work.or2(structural);

for counter0: counter_5bit use entity work.counter_5bit(structural);

for latchrm:	latch use entity work.latch(structural);

for dffcpu1, dffcpu2:	dff use entity work.dff(structural);

for invbusy1, invbusy2 : inverter use entity work.inverter(structural);

signal miss, RH, RM, RMt, clkn, WH, RM18, RH1, write1, T18n, RMT18n, bt0, CPUoutn: std_logic;
signal c4nc3c2, c4nc3, c4nc3c0, CAt1, c4nc3m, c4nc3c2c0n, catt1, catt2, busyt, CPUoutEnt: std_logic;
signal c4nc3c1c0n, CAt2, CAt4, CA1, CA2, c4nc3nc2n, T0, T1, T2, T16, T18: std_logic;
signal c4c3c2, T12, c4c3nc2n, b00, b01, b10, b11, b12, startn, CPUoutEntt: std_logic;
signal stclk, cpu1clk, term1, term2, cacheWrt1, cacheWrt2: std_logic;
signal outputT: std_logic_vector (7 downto 0);
signal count, countn: std_logic_vector(4 downto 0);
signal busybuf : std_logic;

begin
    invmiss	: inverter	port map (hitMiss, miss);
    
    andrm		: and2		port map (read, miss, RMt);
    
    -- keep RM from changing by latching it
    invstart	: inverter	port map (start, startn);
    latchrm	: latch		port map (start, startn, RMt, RM, open);

    -- get CPU output enabled
    andstclk	: and2		port map (start, clk, stClk);
    orbsyClk	: or2		port map (busyt, stClk, cpu1clk);
    dffcpu1	: dff		port map (startn, cpu1clk, term1, open);
    dffcpu2	: dff		port map (busyt, clk, term2, open);
    andcpuout	: and3		port map (term1, term2, read, CPUoutEn);

    -- notify Busy signal
    andb1		: and2		port map (write, T1, write1);
    invt18	: inverter	port map (T18, T18n);
    andRMt18n	: and2		port map (T18n, RM, RMT18n);
    ort0wt1	: or2		port map (T0, write1, bt0);
    orbusy	: or2		port map (bt0, RMT18n, busyt);

    -- use a buffer
    invbusy1	: inverter	port map (busyt, busybuf);
    invbusy2	: inverter	port map (busybuf, busy );

    -- get the state counter set up
    counter0	: counter_5bit	port map (busyt, start, clk, count, countn);

    -- signal for cache write enable
    andcacheWr2	: and3		port map (count(3), count(0), clk, cacheWrt1);
    andcache2	: and3		port map (T1, clk, write, cacheWrt2);
    orcache3	: or2		port map (cacheWrt1, cacheWrt2, cacheWr);

    -- get the byte address for RM
    andb03	: and3		port map (countn(4), count(3), count(0), c4nc3c0);
    andb00	: and2		port map (count(1), c4nc3c0, b00);
    orb01		: or2		port map (b00, T12, b01);
    orb02		: or2		port map (b01, T16, cacheAd(0));


    andb10	: and2		port map (count(0), c4nc3c2, b10);
    andb11	: and2		port map (count(1), c4nc3c2, b11);
    orb12		: or2		port map (b10, b11, b12);
    orb13		: or2		port map (b12, T16, cacheAd(1));

    -- enable the memory adress
    andmemen		:and2		port map (RM, T1, memEn);

    --T0
    andt20		: and3		port map (countn(4), countn(3), countn(2), c4nc3nc2n);
    andt00		: and3		port map (countn(1), countn(0), c4nc3nc2n, T0);

    --T1
    andt10		: and3		port map (countn(1), count(0), c4nc3nc2n, T1);

    --T12
    andt120		: and3		port map (countn(4), count(3), count(2), c4nc3c2);
    andt121		: and3		port map (countn(1), countn(0), c4nc3c2, T12);

    --T16
    andt160		: and3		port map (count(4), countn(3), countn(2), c4c3nc2n);
    andt161		: and3		port map (c4c3nc2n, countn(1), countn(0), T16);

    --T18
    andt181		: and3		port map (count(1), countn(0), c4c3nc2n, T18);

end structural;
