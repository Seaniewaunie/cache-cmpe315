-- Entity: RowEn
-- Architecture: structural
-- Author: mousta1
-- Date: 11/08/2017

library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity rowEn is

  port (
    sel	  : in  std_logic_vector(2 downto 0);
    row0  : out  std_logic;
    row1  : out  std_logic;
    row2  : out  std_logic;
    row3  : out  std_logic;
    row4  : out  std_logic;
    row5  : out  std_logic;
    row6  : out  std_logic;
    row7  : out  std_logic);
end rowEn;

architecture structural of rowEn is

component inverter
  port (
    input	: in std_logic;
    output	: out std_logic);
end component;

component and3
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    input3   : in  std_logic;
    output   : out std_logic);
end component;



for inv_2, inv_1, inv_0: inverter use entity work.inverter(structural);
for and3_1, and3_2, and3_3, and3_4, and3_5, and3_6, and3_7, and3_8: and3 use entity work.and3(structural);


signal in2n, in1n, in0n: std_logic;


begin
  inv_2: inverter port map (sel(2), in2n);
  inv_1: inverter port map (sel(1), in1n);
  inv_0: inverter port map (sel(0), in0n);
  

  and3_1: and3 port map (sel(2), sel(1), sel(0), row7);
  and3_2: and3 port map (sel(2), sel(1), in0n,   row6);
  and3_3: and3 port map (sel(2), in1n,   sel(0), row5);
  and3_4: and3 port map (sel(2), in1n,   in0n,   row4);
  and3_5: and3 port map (in2n,   sel(1), sel(0), row3);
  and3_6: and3 port map (in2n,   sel(1), in0n,   row2);
  and3_7: and3 port map (in2n,   in1n,   sel(0), row1);
  and3_8: and3 port map (in2n,   in1n,   in0n,   row0);

end structural;
