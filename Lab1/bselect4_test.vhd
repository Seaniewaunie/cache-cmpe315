--
-- Entity: bselect4_test 
-- Architecture : test 
-- Author: sgraff2
-- Created On: 09/19/2017
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity bselect4_test is

end bselect4_test;

architecture test of bselect4_test is

component bselect4
  	port (
		B       : in std_logic_vector(3 downto 0);
                S0      : in std_logic;
                S1      : in std_logic;
                output  : out std_logic_vector(3 downto 0));
end component;

for bselect4_1 : bselect4 use entity work.bselect4(structural);
   signal ip1, op : std_logic_vector(3 downto 0);
   signal s0, s1: std_logic;
   signal clock : std_logic;

begin

bselect4_1 : bselect4 port map (ip1, s0, s1, op);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;

io_process: process

  file infile  : text is in "Lab1/bselect4_in.txt";
  file outfile : text is out "Lab1/bselect4_out.txt";
  variable i1, op1 : std_logic_vector(3 downto 0);
  variable sel0, sel1: std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop


    readline(infile,buf);
    read (buf,i1);
    ip1<=i1;

    readline(infile,buf);
    read (buf,sel0);
    s0<=sel0;

    readline(infile,buf);
    read (buf,sel1);
    s1<=sel1;

    wait until falling_edge(clock);

    op1:=op;

    write(buf,op1);
    writeline(outfile,buf);

  end loop;

end process io_process;

end test;
