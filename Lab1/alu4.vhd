--
-- Entity: alu4
-- Architecture : structural
-- Author: sgraff2
-- Created On: 09/19/2017
--
library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity alu4 is
	port (
		A   : in  std_logic_vector(3 downto 0);
		B   : in  std_logic_vector(3 downto 0);
		S0 : in std_logic;
		S1 : in std_logic;
		carryin  : in  std_logic;
		sum      : out std_logic_vector(3 downto 0);
		carryout : out std_logic);
end alu4;

architecture structural of alu4 is

	component adder4
		port (
			input1   : in  std_logic_vector(3 downto 0);
			input2   : in  std_logic_vector(3 downto 0);
			carryin  : in  std_logic;
			sum      : out std_logic_vector(3 downto 0);
			carryout : out std_logic);
	end component;

	component bselect4
		port(
			B       : in std_logic_vector(3 downto 0);
			S0      : in std_logic;
			S1      : in std_logic;
			output  : out std_logic_vector(3 downto 0));	
	end component;	

	for adder4_1 : adder4 use entity work.adder4(structural);
	for bselect4_1 : bselect4 use entity work.bselect4(structural);

	signal temp: std_logic_vector(3 downto 0);

	begin
		bselect4_1 : bselect4 port map ( B, S0, S1, temp);
		adder4_1 : adder4 port map( A, temp, carryin, sum, carryout);


end structural;

----------------------------------------------------------------------------------------------
