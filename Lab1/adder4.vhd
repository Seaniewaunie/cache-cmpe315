--
-- Entity: xor2
-- Architecture : structural
-- Author: cpatel2
-- Created On: 11/11/2003
--
library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity xor2 is

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end xor2;

architecture structural of xor2 is

begin

  output <= input2 xor input1;

end structural;


----------------------------------------------------------------------------------------------------

--
-- Entity: and2
-- Architecture : structural
-- Author: cpatel2
-- Created On: 11/11/2003
--
library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity and2 is

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end and2;

architecture structural of and2 is

begin

  output <= input2 and input1;

end structural;

--------------------------------------------------------------------------------------------------

--
-- Entity: or3
-- Architecture : structural
-- Author: cpatel2
-- Created On: 11/11/2003
--
library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity or3 is

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    input3   : in  std_logic;
    output   : out std_logic);
end or3;

architecture structural of or3 is

begin

  output <= input3 or input2 or input1;

end structural;


---------------------------------------------------------------------------------------------

--
-- Entity: adder1
-- Architecture : structural
-- Author: cpatel2
-- Created On: 10/21/2004
--
library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity adder1 is

  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    carryin  : in  std_logic;
    sum      : out std_logic;
    carryout : out std_logic);
end adder1;

architecture structural of adder1 is

component xor2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

component and2
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    output   : out std_logic);
end component;

component or3
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    input3   : in  std_logic;
    output   : out std_logic);
end component;


for xor2_1, xor2_2: xor2 use entity work.xor2(structural);
for and2_1, and2_2, and2_3: and2 use entity work.and2(structural);
for or3_1: or3 use entity work.or3(structural);

signal temp1, temp2, temp3, temp4: std_logic;

begin

xor2_1: xor2 port map (input1, input2, temp1);
xor2_2: xor2 port map (carryin, temp1, sum);

and2_1: and2 port map (input1, input2, temp2);
and2_2: and2 port map (input1, carryin, temp3);
and2_3: and2 port map (input2, carryin, temp4);

or3_1: or3 port map (temp2, temp3, temp4, carryout);

end structural;

----------------------------------------------------------------------------------------------

--
-- Entity: adder4
-- Architecture : structural
-- Author: cpatel2
-- Created On: 10/21/2004
--
library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity adder4 is

  port (
    input1   : in  std_logic_vector(3 downto 0);
    input2   : in  std_logic_vector(3 downto 0);
    carryin  : in  std_logic;
    sum      : out std_logic_vector(3 downto 0);
    carryout : out std_logic);
end adder4;

architecture structural of adder4 is

component adder1
  port (
    input1   : in  std_logic;
    input2   : in  std_logic;
    carryin  : in  std_logic;
    sum      : out std_logic;
    carryout : out std_logic);
end component;

for adder1_1, adder1_2, adder1_3, adder1_4: adder1 use entity work.adder1(structural);

signal ctemp: std_logic_vector(2 downto 0);

begin

adder1_1: adder1 port map (input1(0), input2(0), carryin, sum(0), ctemp(0));
adder1_2: adder1 port map (input1(1), input2(1), ctemp(0), sum(1), ctemp(1));
adder1_3: adder1 port map (input1(2), input2(2), ctemp(1), sum(2), ctemp(2));
adder1_4: adder1 port map (input1(3), input2(3), ctemp(2), sum(3), carryout);

end structural;

----------------------------------------------------------------------------------------------
