--
-- Entity: bselect4
-- Architecture : structural 
-- Author: sgraff2
-- Created On: 09/18/17 at 12:34
--
library STD;
library IEEE;
use IEEE.std_logic_1164.all;

entity bselect4 is
	port(
		B	: in std_logic_vector(3 downto 0);
		S0	: in std_logic;
		S1	: in std_logic;
		output	: out std_logic_vector(3 downto 0));
end bselect4;

architecture structural of bselect4 is

	component inverter
		port(
			input	: in std_logic;
			output	: out std_logic);
	end component;

	component and2
		port(
			input1	: in std_logic;
			input2	: in std_logic;
			output	: out std_logic);
	end component;

	component or2
		port(
			input1	: in std_logic;
			input2	: in std_logic;
			output	: out std_logic);
	end component;
	
	for i1 : inverter use entity work.inverter(structural);
	for i2 : inverter use entity work.inverter(structural);
	for i3 : inverter use entity work.inverter(structural);
	for i4 : inverter use entity work.inverter(structural);

	for a1 : and2 use entity work.and2(structural);
	for a2 : and2 use entity work.and2(structural);
	for a3 : and2 use entity work.and2(structural);
	for a4 : and2 use entity work.and2(structural);
	for a5 : and2 use entity work.and2(structural);
	for a6 : and2 use entity work.and2(structural);
	for a7 : and2 use entity work.and2(structural);
	for a8 : and2 use entity work.and2(structural);

	for o1 : or2 use entity work.or2(structural);
	for o2 : or2 use entity work.or2(structural);
	for o3 : or2 use entity work.or2(structural);
	for o4 : or2 use entity work.or2(structural);

	signal notB : std_logic_vector(3 downto 0);
	signal X : std_logic_vector(3 downto 0);
	signal Z : std_logic_vector(3 downto 0);

begin
	-- not B
	i1: inverter port map( B(0), notB(0));
	i2: inverter port map( B(1), notB(1));
	i3: inverter port map( B(2), notB(2));
	i4: inverter port map( B(3), notB(3));


	-- s0 and B
	a1: and2 port map( S0, B(0), X(0));
	a2: and2 port map( S0, B(1), X(1));
	a3: and2 port map( S0, B(2), X(2));
	a4: and2 port map( S0, B(3), X(3));

	-- s1 and not B	
	a5: and2 port map( S1, notB(0), Z(0));
	a6: and2 port map( S1, notB(1), Z(1));
	a7: and2 port map( S1, notB(2), Z(2));
	a8: and2 port map( S1, notB(3), Z(3));
	
	-- X or Z
	o1: or2 port map( X(0), Z(0), output(0)); 	
	o2: or2 port map( X(1), Z(1), output(1)); 	
	o3: or2 port map( X(2), Z(2), output(2)); 	
	o4: or2 port map( X(3), Z(3), output(3)); 	

end structural;
