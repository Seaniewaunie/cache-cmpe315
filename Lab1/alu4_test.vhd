--
-- Entity: alu4_test 
-- Architecture : test 
-- Author: sgraff2
-- Created On: 09/19/2017
--
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_textio.all;
use STD.textio.all;

entity alu4_test is

end alu4_test;

architecture test of alu4_test is

component alu4
  	port (
		A   : in  std_logic_vector(3 downto 0);
                B   : in  std_logic_vector(3 downto 0);
                S0 : in std_logic;
                S1 : in std_logic;
                carryin  : in  std_logic;
                sum      : out std_logic_vector(3 downto 0);
                carryout : out std_logic);
end component;

for alu4_1 : alu4 use entity work.alu4(structural);
   signal ip1, ip2, op : std_logic_vector(3 downto 0);
   signal s0, s1, cin, cout: std_logic;
   signal clock : std_logic;

begin

alu4_1 : alu4 port map (ip1, ip2, s0, s1, cin, op, cout);

clk : process
   begin  -- process clk

    clock<='0','1' after 5 ns;
    wait for 10 ns;

  end process clk;

io_process: process

  file infile  : text is in "Lab1/alu4_in.txt";
  file outfile : text is out "Lab1/alu4_out.txt";
  variable i1, i2, op1 : std_logic_vector(3 downto 0);
  variable sel0, sel1, ci, co: std_logic;
  variable buf : line;

begin

  while not (endfile(infile)) loop


    readline(infile,buf);
    read (buf,i1);
    ip1<=i1;

    readline(infile,buf);
    read (buf,i2);
    ip2<=i2;

    readline(infile,buf);
    read (buf,sel0);
    s0<=sel0;

    readline(infile,buf);
    read (buf,sel1);
    s1<=sel1;

    readline(infile, buf);
    read (buf, ci);
    cin<=ci;

    wait until falling_edge(clock);

    op1:=op;
    co:=cout;

    write(buf,op1);
    writeline(outfile,buf);

    write(buf, co);
    writeline(outfile, buf);

  end loop;

end process io_process;

end test;
